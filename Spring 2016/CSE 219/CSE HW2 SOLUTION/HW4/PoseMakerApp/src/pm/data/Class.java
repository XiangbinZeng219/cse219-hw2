/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;

import java.util.ArrayList;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import pm.data.DataManager;
import pm.file.FileManager;
import pm.gui.Workspace;
import saf.AppTemplate;
import saf.ui.AppGUI;


/**
 *
 * @author Xiangbin
 */
public class Class { 
    AppTemplate app;
    //Workspace workspace= (Workspace)app.getWorkspaceComponent();
    AppGUI gui;
    public String parent="null"; 
    String name="null";
    String Package="null";
    public double x;
    public double y;
    public ArrayList<Method> methods=new  ArrayList();
    public ArrayList<Variable> variables=new  ArrayList();
    public ArrayList<String> improt=new ArrayList();
 
    public  GraphicsContext gc;
    public  Canvas layer=new Canvas(120,180);
    DropShadow ds = new DropShadow();
    


    public Class() {}
    public   Class(String name){
    this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackage() {
        return Package;
    }

    public void setPackage(String Package) {
        this.Package = Package;
    }

    
    public Canvas drawShapes(){    
        gc=layer.getGraphicsContext2D();   
	String outlineColor = "#000000";
        gc.beginPath();
	gc.setStroke(Paint.valueOf(outlineColor));
	gc.setLineWidth(3);
        gc.rect(0, 0, 115, 60);
	gc.stroke();
        gc.beginPath();
	gc.setStroke(Paint.valueOf(outlineColor));
	gc.setLineWidth(3);
        gc.rect(0, 0+60, 115, 60);
	gc.stroke();
        gc.beginPath();
	gc.setStroke(Paint.valueOf(outlineColor));
	gc.setLineWidth(3);
        gc.rect(0, 0+120, 115, 60);
	gc.stroke();
        gc.fillText(name,0+5,0+20);
        return layer;
    }
    
    public void getEffect(){
        ds.setOffsetY(7.5);
        ds.setOffsetX(7.5);
        ds.setColor(Color.GRAY);
        layer.setEffect(ds);
    }
    
    public void loseEffect(){
        layer.setEffect(null);
    }
    
    public void rewrite(){
     gc.beginPath();
     gc.clearRect(3, 3, 113, 58);
     gc.fillText(name,0+5,0+20);
    }
    
    public void setItLayer(){
        layer.setOnMouseDragged(e->{
        x=e.getSceneX();
        y=e.getSceneY();
        layer.setTranslateX(e.getX());
        layer.setTranslateY(e.getY());
        });
    }
       public String toString(){
           String a="name:"+name+ " Package:"+getPackage()+" X:"+x+" Y:"+y+ "Variables: ";
           for(int v=0;v< variables.size();v++){
               a+= variables.get(v).toString()+" ";
           }
           a+=" Methods:";
           for(int v=0;v< methods.size();v++){
               a+= methods.get(v).toString()+" ";
           }
           a+=" improts:";
           for(int v=0;v<improt.size();v++){
                a+= improt.get(v);
           }
              return a;
          }
    
    
    }
