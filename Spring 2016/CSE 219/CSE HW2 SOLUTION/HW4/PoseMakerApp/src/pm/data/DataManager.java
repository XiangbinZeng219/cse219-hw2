package pm.data;

import javafx.collections.ObservableList;
import javafx.scene.canvas.Canvas;
import pm.ClassDesignerMaker;
import pm.data.Class;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    //There are the classes to draw
    ObservableList<Class> Class;
    
    //This is the class being sized but not yet added
    Class newClass;
    
    //SlectedClass
    Class seletedClass;
       
    //x,y where you are
    double x;
    double y;
    
    //Current state of the app
    ClassDesignerMaker state;
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        
        //No shape class out as seleted
        newClass=null;
        seletedClass=null;
        
        
        
        
        
    }
    
    public ObservableList<Class> getClass(){
        return Class;
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {

    }
}
