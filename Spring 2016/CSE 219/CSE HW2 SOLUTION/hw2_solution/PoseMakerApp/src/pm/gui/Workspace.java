package pm.gui;

import java.io.IOException;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import pm.PropertyType;
import pm.data.Class;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author XiangbinZeng
 * @version 1.0
 * 
 */
public class Workspace extends AppWorkspaceComponent {
    //statc final string
        static final String BORDERED_PANE = "color_chooser_pane";
        static final String BUTTON_STYLE= "tag_button";
        static final String SHOW_PANE="edit_toolbar";
        static final String NAME="heading_label";
        static final String OTHER_SUBHEADING="prompt_text_field";
    
    //All panes
    HBox TopPane;
    HBox filePane;
    HBox ToolPane;
    HBox SizePane;
    HBox botPane;
    GridPane classPane;
    Pane classShowPane;
    VBox workspaceBorderPane;

    //Variable for WorkSpace 
//    Button Save;
//    Button Load;
//    Button SaveAs;
    Button Photo;
    Button Code;
//    Button Exit;
    Button Select;
    Button Resize;
    Button AddClass;
    Button Remove;
    Button Undo;
    Button Redo;
      
    Button ZoomIn;
    Button ZoomOut;
    
    CheckBox Gird;
    CheckBox Snap;
    
//List of Class
    Class newClass;
    ArrayList<Class> classes;
    Label Name;
    Label Package;
    Class SelectedClass;
    TextField NameTextField;
    TextField NameOfPackage;
//Canvas
    public Canvas canvas;
    Group group;
    

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        
       classes=new ArrayList<Class>();
        
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        filePane=new HBox();
        ToolPane=new HBox();
        TopPane=new HBox();
        SizePane=new HBox();
        classShowPane=new Pane();
        classPane=new GridPane();
        botPane=new HBox();
        
        //set the size of the pane 
        classShowPane.setPrefSize(1200, 900);
        classPane.setPrefSize(600, 900);
        filePane.setPrefWidth(300);
        ToolPane.setPrefWidth(900);
        SizePane.setPrefWidth(600);
        
        //put panes to top pane
        TopPane.getChildren().addAll(filePane,ToolPane,SizePane);
        botPane.getChildren().addAll(classShowPane,classPane);
 
        //add the pane to the border
        workspaceBorderPane = new VBox();
        workspaceBorderPane.getChildren().addAll(TopPane,botPane);
        

        workspace = new Pane();
        workspace.getChildren().add(workspaceBorderPane);
       
        
        //add Button to the pane
        Photo=new Button("Photo");  
        Code=new Button("Code");
        filePane.getChildren().addAll(Photo,Code);
   
        Select=new Button("Select");
        Resize=new Button("Resize");
        AddClass=new Button("AddClass");
        Remove=new Button("Remove");
        Undo=new Button("Undo");
        Redo=new Button("Redo");
        ToolPane.getChildren().addAll(Select,Resize,AddClass,Remove,Undo,Redo);
        
        ZoomIn=new Button("ZoomIn");
        ZoomOut=new Button("ZoomOut");
        Gird=new CheckBox("Gird");
        Snap=new CheckBox("Snap");
        SizePane.getChildren().addAll(ZoomIn,ZoomOut,Gird,Snap);
    
        Name=new Label("Class Name:    ");

        NameTextField=new TextField();
        Package=new Label("Package:    ");

        NameOfPackage=new TextField();
        Name.getStyleClass().add(NAME);
        Package.getStyleClass().add(OTHER_SUBHEADING);
        

        NameTextField.setOnKeyPressed(new EventHandler<KeyEvent>(){
            public void handle(KeyEvent e){
                if(e.getCode()==KeyCode.ENTER){
                    if(SelectedClass!=null){
                    SelectedClass.setName(NameTextField.getText());
                    SelectedClass.rewrite();

                    }
                }
            }
        });
        
        NameOfPackage.setOnKeyPressed(new EventHandler<KeyEvent>(){
       
            public void handle(KeyEvent e){
                if(e.getCode()==KeyCode.ENTER){
                    if(SelectedClass!=null){
                    SelectedClass.setPackage(NameTextField.getText());

                    }
                }
            }
        });
        
        classPane.add(Name, 1, 0);
        classPane.add(NameTextField,2,0);
        classPane.add(Package,1,1);
        classPane.add(NameOfPackage, 2, 1);
        

        
 
        
        
        canvas = new Canvas(1200,900);
        group=new Group();
      //  group.getChildren().add(canvas);
        
        

       addAllStyleToButton(); 

        
        
       AddClass.setOnAction(e->{
           AddClass.setDisable(true);
           Select.setDisable(false);
            for(int n=0;n<classes.size();n++){
                classes.get(n).layer.setOnMouseDragged(null);
            }
            classShowPane.setOnMouseClicked((MouseEvent event) -> {        
                Canvas a;
             newClass= new Class();     
             classes.add(newClass);
               if(SelectedClass!=null){
                    SelectedClass.loseEffect();
                }
                SelectedClass=newClass;
                SelectedClass.getEffect(); 

              a=newClass.drawShapes();
              a.setLayoutX(event.getX());
              a.setLayoutY(event.getY());
              newClass.y=event.getX();
              newClass.x=event.getX();
              classShowPane.getChildren().add(a);
              System.out.print(newClass.x);     
       });  
        
    });
       
       
    Select.setOnAction(e->{
           AddClass.setDisable(false);
           Select.setDisable(true);
           classShowPane.setOnMouseClicked(a->{
               for(int n=0;n<classes.size();n++){
                  classes.get(n).setItLayer();
                  if(classes.get(n).layer.getLayoutX()+120>=a.getSceneX()&&
                          classes.get(n).layer.getLayoutX()<a.getSceneX()&&
                          classes.get(n).layer.getLayoutY()+180>=a.getSceneY()&&
                          classes.get(n).layer.getLayoutY()<=a.getSceneY()-30){     
                      classes.get(n).getEffect();
                      if(SelectedClass!=null){
                      SelectedClass.loseEffect();}
                      SelectedClass= classes.get(n);
                      
               }
           } });

       });
       
       
       Remove.setOnAction(e->{
       if(SelectedClass!=null){    
       classes.remove(SelectedClass);
       classShowPane.getChildren().remove(SelectedClass.layer);
       SelectedClass=null;}
       });

       
    }

    
    
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    //@Override;
       public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        filePane.getStyleClass().add(BORDERED_PANE);
        ToolPane.getStyleClass().add(BORDERED_PANE);
        SizePane.getStyleClass().add(BORDERED_PANE);
        classPane.getStyleClass().add(BORDERED_PANE);
//        classShowPane.getStyleClass().add(SHOW_PANE);
    }
    
    public void addAllStyleToButton(){
         Photo.getStyleClass().add(BUTTON_STYLE);
         Code.getStyleClass().add(BUTTON_STYLE);
         Select.getStyleClass().add(BUTTON_STYLE);
         Resize.getStyleClass().add(BUTTON_STYLE);
         AddClass.getStyleClass().add(BUTTON_STYLE);
         Remove.getStyleClass().add(BUTTON_STYLE);
         Undo.getStyleClass().add(BUTTON_STYLE);
         Redo.getStyleClass().add(BUTTON_STYLE);
         ZoomIn.getStyleClass().add(BUTTON_STYLE);
         ZoomOut.getStyleClass().add(BUTTON_STYLE);
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    //@Override
    public void reloadWorkspace() {
       classes=new ArrayList<Class>();
       classShowPane.getChildren().clear();
    }
    
    public Canvas getCanvas(){
        return canvas;
    }
}
